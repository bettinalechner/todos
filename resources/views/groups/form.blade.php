<div class="row form-group">
   <div class="col-md-3">
     {!! Form::label('name') !!}
     {!! Form::text('name', null, ['class' => 'form-control']) !!}
   </div>
</div>

<div class="row">
  <div class="col-md-3">
    <p>{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}</p>
  </div>
</div>