@extends('app')

@section('content')

<h1>Groups</h1>

<table class="table">
  <tr>
    <th>Name</th>
    <th></th>
  </tr>
@foreach($groups as $group)
  <tr>
      <td><a href="{{ action('GroupController@show', [$group->id]) }}">{{ $group->name }}</a></td>
    <td>
      {!! Form::model($group, [
      'method' => 'DELETE',
      'action' => [
      'GroupController@destroy', $group->id
      ]
       ]) !!}
      <a href="{{ action('GroupController@edit', [$group->id]) }}" class="btn btn-primary">Edit</a>
      <button type="submit" class="btn btn-danger">Delete</button>
      {!! Form::close() !!}
    </td>
  </tr>
@endforeach
</table>

@endsection