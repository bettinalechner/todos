@extends('app')

@section('content')

<h1>Add a new Group</h1>

@if (count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>
@endif

{!! Form::open(['action' => 'GroupController@store']) !!}

@include('groups.form')

{!! Form::close() !!}

@endsection