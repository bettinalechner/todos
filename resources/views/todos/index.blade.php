@extends('app')

@section('content')

<h1>To Dos</h1>

<table class="table">
  <tr>
    <th>Task</th>
    <th>Completed</th>
    <th>Due Date</th>
    <th></th>
  </tr>
@foreach($todos as $todo)
  <tr>
    <td>{{ $todo->task }}</td>
    <td>{{ $todo->completed }}</td>
    <td>{{ $todo->due_date }}</td>
    <td>
      {!! Form::model($todo, [
      'method' => 'DELETE',
      'action' => [
      'ToDoController@destroy', $todo->id
      ]
       ]) !!}
      <a href="{{ action('ToDoController@edit', [$todo->id]) }}" class="btn btn-primary">Edit</a>
      <button type="submit" class="btn btn-danger">Delete</button>
      {!! Form::close() !!}
    </td>
  </tr>
@endforeach
</table>

@endsection