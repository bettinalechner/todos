<div class="row form-group">
   <div class="col-md-3">
     {!! Form::label('task') !!}
     {!! Form::text('task', null, ['class' => 'form-control']) !!}
   </div>
  <div class="col-md-3">
    {!! Form::label('due_date') !!}
    {!! Form::text('due_date', date('m/d/Y'), ['class' => 'form-control']) !!}
  </div>
  <div class="col-md-3">
    {!! Form::label('completed') !!}
    {!! Form::select('completed', [false => 'No', true => 'Yes'], null, ['class' => 'form-control']) !!}
  </div>
</div>

<div class="row">
  <div class="col-md-3">
    <p>{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}</p>
  </div>
</div>