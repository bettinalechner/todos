@extends('app')

@section('content')

<h1>{{ $todo->task }}</h1>
<p>Due: {{ $todo->due_date }}</p>
<p>Completed: {{ $todo->completed }}</p>

@endsection