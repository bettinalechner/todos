@extends('app')

@section('content')

<h1>Edit a To Do</h1>

{!! Form::model($todo, [
  'action' => ['ToDoController@update', $todo->id],
  'method' => 'PATCH'
]) !!}
@include('todos.form')
{!! Form::close() !!}

@endsection