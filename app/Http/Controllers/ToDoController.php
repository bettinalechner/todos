<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ToDo;
use App\Group;

class ToDoController extends Controller
{
  public function index() {
    $todos = ToDo::all();
    return view('todos.index', compact('todos'));
  }

  public function create($groupId) {
    return view('todos.create', compact('groupId'));
  }

  public function store(Requests\ToDoRequest $request, $groupId) {
    $group = Group::find($groupId);
    $todo = ToDo::create($request->all());
    $group->toDos()->save($todo);

    return redirect()->route('groups.show', $group->id);
  }

  public function show($id) {
    $todo = ToDo::find($id);
    return view('todos.show', compact('todo'));
  }

  public function edit($id) {
    $todo = ToDo::find($id);
    return view('todos.edit', compact('todo'));
  }

  public function update(Requests\ToDoRequest $request, $id) {
    $todo = ToDo::find($id);
    $todo->update($request->all());
    return redirect()->route('todos.index');
  }

  public function destroy($id) {
    $todo = ToDo::find($id);
    $todo->delete();
    return redirect()->route('todos.index');
  }
}
