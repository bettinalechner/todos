<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToDo extends Model
{
    protected $fillable = [
      'task',
      'due_date',
      'completed',
      'group_id',
    ];

    public function group() {
        return $this->belongsTo('App\Group');
    }

  protected $dates = ['due_date'];

  public function getCompletedAttribute($value) {
    if ($value == true) {
      return 'Yes';
    } else {
      return 'No';
    }
  }

  public function setDueDateAttribute($value) {
    $this->attributes['due_date'] = date_create($value);
  }

  public function getDueDateAttribute($value) {
    $date = date_create_from_format('Y-m-d', $value);
    return $date->format('m/d/Y');
  }
}
